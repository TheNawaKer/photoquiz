var db = openDatabase('PhotoQuiz','1.0','Base de donnees pour le jeu',3000)
var alpha = "abcdefghijklmnopqrstuvwxyz";
var partie = {
	word: "",
	serie: 0,
	letterList: "",
	score: 0,
	answer: "",
	dateDebut: null,
	init: function(){
		this.serie = localStorage.currentSerie;
		db.readTransaction(function(tx){
			tx.executeSql('SELECT mot FROM MOTS WHERE id=?', [partie.serie], function(tx, results){
				partie.answer = "";
				partie.word = results.rows.item(0)['mot'].toLowerCase();
				partie.letterList = partie.word;
				for(var i = 0,len = partie.letterList.length; len+i<10;i++){
					partie.letterList += alpha.charAt(Math.floor(Math.random() * alpha.length));
				}
				partie.score = parseInt(localStorage.score);
				shuffle(partie.letterList);
				setPictures();
				setLetters();
				partie.dateDebut = new Date();
			}, onError);
		});
	}	
}
var insertNb = 0;
var maxSerie = 3;

function onError(e){
	alert("une erreur c'est produite");
}

function tableInit(tx,results){
	insertNb++;
	if(insertNb==3)
		partie.init(); 
}

if(!localStorage.firstLaunch){
	db.transaction(function(tx){
		tx.executeSql('DROP TABLE IF EXISTS MOTS', [], tableInit, onError);
		tx.executeSql('CREATE TABLE MOTS(id INT,mot VARCHAR(10))', [], tableInit, onError);
		tx.executeSql('INSERT INTO MOTS (id,mot) VALUES (?, ?)', [1,"LUMIERE"], tableInit, onError);
		tx.executeSql('INSERT INTO MOTS (id,mot) VALUES (?, ?)', [2,"AMOUR"], tableInit, onError);
		tx.executeSql('INSERT INTO MOTS (id,mot) VALUES (?, ?)', [3,"ARGENT"], tableInit, onError);
		localStorage.firstLaunch = true;
		localStorage.currentSerie = 1;
		localStorage.endSerie = maxSerie;
		localStorage.score = 0;
		partie.serie = 1;
		partie.init();
	});	
}else{
	partie.init();
}

var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
var minSize = x < y ? x : y;

function setPictures(){
	var photoCpt = 1;
	$("#pictures canvas").each(function() { 
			var canvas = this;	
			img = new Image();
			img.onload = function(e){
				var ctx = canvas.getContext('2d');
				var size = minSize - 10 < 128 ? minSize/2 - 10 : 128;
				canvas.width=size
				canvas.height=size
				ctx.drawImage(this, 0, 0, size, size);
			}
			img.src = "img/series/"+partie.serie+"/photo"+photoCpt+".jpg";	
			photoCpt++;			
	});  
}

function zoom(id){	
	var canvas = document.createElement("canvas");	
	img = new Image();
	img.onload = function(e){
		var ctx = canvas.getContext('2d');
		canvas.width=minSize
		canvas.height=minSize
		ctx.drawImage(this, 0, 0, minSize, minSize);
	}
	img.src = "img/series/"+partie.serie+"/photo"+id+".jpg";
	canvas.id = "zoom";
	canvas.onclick = function(e){
		$("#zoom").remove();
		$("#content").show();
	}
	$("#page").append(canvas);
	$("#content").hide();
}

function setLetters(){
	$('#letters canvas').remove();
	for (var i = 0, len = partie.letterList.length; i < len; i++) {		
		img = new Image();
		img.id = partie.letterList[i];
		img.title = i;
		img.onload = function(e){
			var canvas = document.createElement("canvas");
			var ctx = canvas.getContext('2d');
			var size = 50;
			canvas.width=size
			canvas.height=size
			ctx.drawImage(this, 0, 0, size, size);
			canvas.id = this.id+this.title;
			canvas.title = this.id;
			canvas.onclick = function(e){
				addLetter(this,this.title);
			}
			$("#letters").append(canvas);
		}
		img.src = "img/alphabet/"+partie.letterList[i]+".png";	
	}
}

function addLetter(canvasLetter, letter){
	partie.answer+=letter
	$("#inAnswer").val(partie.answer);
	$("#"+canvasLetter.id).hide();
}

function updateAnswer(){
	partie.answer = $("#inAnswer").val();
}

function reset(){
	partie.answer = "";
	$("#inAnswer").val('');
	$("#letters canvas").each(function(){
		$("#"+this.id).show();
	});
}

function $random(min, max){
	return Math.floor(Math.random() * (max - min + 1) + min);
};

function shuffle(sentence) {
    return sentence
	.split("")
	.sort(function(a,b) {
	    if($random(0,100)%2 == 0) {
	        return 1;
	    } else {
	        return -1;
	    }
	}).join("");
}

function check(){
	if(partie.answer==partie.word){
		var dateFin = new Date();
		var temps = dateFin - partie.dateDebut;
		temps = new Date(temps);
		var secondes = temps.getSeconds();
		if(secondes < 5){
			partie.score += 50;
		}else if (secondes < 10){
			partie.score += 40;
		}else if (secondes < 20){
			partie.score += 20;
		}else if (secondes < 30){
			partie.score += 10;
		}else{
			partie.score += 1;
		}
		localStorage.score = partie.score;
		$("#inAnswer").val('');
		if(partie.serie==localStorage.endSerie){
			$("#popupTitle").text("Fin de Jeu");
			$("#popupText").text("Jeu terminé, bravo !\nVotre score final est de: "+partie.score);
			$.mobile.changePage('#popup', {
	            transition: 'pop',
	            changeHash: true,
	            role: 'dialog'
        	});
			restart();
		}else{
			partie.serie++;
			localStorage.currentSerie++;			
			$('#letters canvas').remove();
			partie.init();
		}
	}
}

function skip(){
	if(partie.serie==localStorage.endSerie){
		$("#popupTitle").text("Fin de Jeu");
		$("#popupText").text("Réponse: "+partie.word+".\n Votre score est de "+partie.score+"\n Jeu termine !");
		$.mobile.changePage('#popup', {
	        transition: 'pop',
	        changeHash: true,
	        role: 'dialog'
		});
		restart();
	}else{
		$("#popupTitle").text("Solution:");
		$("#popupText").text("Il fallait trouver \""+partie.word+"\".\n Votre score est de "+partie.score);
		$.mobile.changePage('#popup', {
	        transition: 'pop',
	        changeHash: true,
	        role: 'dialog'
		});
		partie.serie++;
		localStorage.currentSerie++;			
		$('#letters canvas').remove();
		partie.init();
	}
	
}

function restart(){
	localStorage.score = 0;
	localStorage.currentSerie = 1;
	partie.score = 0;
	partie.serie = 1;
	$("#inAnswer").val('');
	partie.init();
}

function closePopup(){
	$('#popup').dialog('close');
}
